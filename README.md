# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Discord Bot
* 1.0.0


### How do I get set up? ###

* Download
* -help

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* ☭𝔗𝔥𝔞𝔱 𝔒𝔫𝔢𝔊𝔲𝔶1375☭#5696 on discord


### Code ###

Put into .js file


const Discord = require("discord.js");


const client = new Discord.Client();
 
const config = require("./config.json");


client.on("ready", () => {
  
  console.log(`Bot has started, with ${client.users.size} users, in ${client.channels.size} channels of ${client.guilds.size} guilds.`); 
  
  client.user.setActivity(`-help In ${client.guilds.size} Servers with ${client.users.size} members`);
});

client.on("guildCreate", guild => {
  
  console.log(`New guild joined: ${guild.name} (id: ${guild.id}). This guild has ${guild.memberCount} members!`);
  client.user.setActivity(`-help In ${client.guilds.size} Servers with ${client.users.size} members`);
});

client.on("guildDelete", guild => {
  
  console.log(`Bot has been kicked/banned from the server: ${guild.name} (id: ${guild.id})`);
  client.user.setActivity(`-help In ${client.guilds.size} Servers with ${client.users.size} members`);
});


client.on("message", async message => {
  
  
  
  if(message.author.bot) return;
  
  
  if(message.content.indexOf(config.prefix) !== 0) return;
  
  
  const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();
  
  if(command === "botinfo") {

    const m = await message.channel.send("Finding Info...");
    m.edit(`Information: Bot Name: TTS. Version: 1.0.9 , Bot Owner: ☭𝔗𝔥𝔞𝔱 𝔒𝔫𝔢𝔊𝔲𝔶1375☭#5696`);
  }
  
  if(command === "botinfo") {

    const m = await message.channel.send("Finding Info...");
    m.edit(`https://cdn.discordapp.com/avatars/453687701208825856/bf598a14544ae5881977c96e7567da54.png?size=128`);
  }  

  if(command === "help") {

    const m = await message.channel.send("Helping");
    m.edit(`
    Note: To use 'admin' commands you have to make a role specifically called "TTS_Perms". 
    Commands: 
    serverinfo: (gives info about the server the bot was developed in not the one you are currently in). 
    botinfo: gives you info about the bot. 
    googlethis: google things (ex: !googlethis discord_bots_tts). 
    musiclistings: look up and play music in the voice chat you are in. 
    kick: kicks user (ex: kick @.☭𝔗𝔥𝔞𝔱 𝔒𝔫𝔢𝔊𝔲𝔶1375☭#5696 without the period after the @) 
    ban: bans user same usage as kick.
    say: says message user sends. 
    purge: purges or gets rid of messages from 2 to 100. invite: gives user invite to the bots server. 
    invite: gives an invite to the bots server.
    addthebot: gives you the OAuth2 link.
    A few "secret" commands are hidden, hats off to you if you find them ;)
    `);
  }

  if(command === "googlethis") {

    const m = await message.channel.send("Googling");
    m.edit(`This Command is Still in the Works.`);
  }

  if(command === "quote") {

    const m = await message.channel.send("Starting Over is harder than starting up, like deleting your homework the day before it was due, as I have, or betting all your winnings at a casino on red and it came up black, starting over is harder than starting up, remember that. -Bennett Foddy");
  }

if(command === "quote") {

    const m = await message.channel.send("https://youtu.be/rXmBPIJZfVA");
}

if(command === "addthebot") {

    const m = await message.channel.send("https://discordapp.com/api/oauth2/authorize?client_id=453687701208825856&permissions=8&scope=bot")
}

if(command === "addthebot") {

  const m = await message.channel.send("Thanks!")
}

  if(command === "secrets") {

    const m = await message.channel.send("Loading Suprising Secrets ;)");
    m.edit(`This Bot was thought of within 2 minutes and was fully coded to the point that it worked in about 2 days`);
  }

  if(command === "invite") {

    const m = await message.channel.send("Getting Invite");
    m.edit(`https://discord.gg/rUJWbwX`);
  }

  if(command === "musiclistings") {

    const m = await message.channel.send("Finding Listings");
    m.edit(`This Command is Still in the Works as Well.`);
  }

  if(command === "say") {
    
    const sayMessage = args.join(" ");
   
    message.delete().catch(O_o=>{}); 
    
    message.channel.send(sayMessage);
  }
  
  if(command === "kick") {
   
    if(!message.member.roles.some(r=>["TTS_Perms"].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
    

    let member = message.mentions.members.first() || message.guild.members.get(args[0]);
    if(!member)
      return message.reply("Please mention a valid member.");
    if(!member.kickable) 
      return message.reply("I cannot kick this user. Do they have a higher role? Do I have kick permissions?");
    

    let reason = args.slice(1).join(' ');
    if(!reason) reason = "No reason provided.";
    

    await member.kick(reason)
      .catch(error => message.reply(`Sorry ${message.author} I couldn't kick because of : ${error}`));
    message.reply(`${member.user.tag} has been kicked by ${message.author.tag} because: ${reason}`);

  }
  
  if(command === "ban") {
  
    if(!message.member.roles.some(r=>["TTS_Perms"].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
    
    let member = message.mentions.members.first();
    if(!member)
      return message.reply("Please mention a valid member of this server");
    if(!member.bannable) 
      return message.reply("I cannot ban this user! Do they have a higher role? Do I have ban permissions?");

    let reason = args.slice(1).join(' ');
    if(!reason) reason = "No reason provided";
    
    await member.ban(reason)
      .catch(error => message.reply(`Sorry ${message.author} I couldn't ban because of : ${error}`));
    message.reply(`${member.user.tag} has been banned by ${message.author.tag} because: ${reason}`);
  }
  
  if(command === "purge") {
    
    const deleteCount = parseInt(args[0], 10);
    
    if(!message.member.roles.some(r=>["TTS_Perms"].includes(r.name)) )
      return message.reply("Sorry, you don't have permissions to use this!");
    if(!deleteCount || deleteCount < 2 || deleteCount > 100)

      return message.reply("Please list a number between 2 and 100 for the number of messages to be deleted in this message purge.");
    
   
    const fetched = await message.channel.fetchMessages({limit: deleteCount});
    message.channel.bulkDelete(fetched)
      .catch(error => message.reply(`Couldn't delete messages because of this error: ${error}`));
  
    }
});

client.login(config.token);

(Put into config.json file)

{ 
    "token"  : "Token_Here",
    "prefix" : "-"
  }
  
  
  ---------